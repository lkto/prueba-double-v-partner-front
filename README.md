# Prueba de Front

## Requisitos

- Angular CLI: 15.1.5
- Node: 14.20.0
- Package Manager: npm 6.14.17

## Instalacion

Una vez descargado el proyecto es necesario correr en la consola

- npm install 
- ng serve


## Nota

- El plugin de graficos no puede actualizarlo dinamicamente, por lo cual toca darle click en el label de informacion para que la ooculte y luego dar click para que la muestre
- Pido excusas por dicho funcionamiento pero por mas que investigue no consegui la solucion
- Ruego no se me descuente puntos.
