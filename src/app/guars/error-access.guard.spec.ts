import { TestBed } from '@angular/core/testing';

import { ErrorAccessGuard } from './error-access.guard';

describe('ErrorAccessGuard', () => {
  let guard: ErrorAccessGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ErrorAccessGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
