
export interface User {
  items: UserInformation[],
}

export interface UserInformation {
  id: string;
  avatar_url: string;
  url: string;
  followers_url: string;
  subscriptions_url: string;
  login: string;
  score: number;
  name: string;
  followers: string,
  public_repos : string,
  location : string,
  following : string
}