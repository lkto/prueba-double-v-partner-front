import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserInformation } from '../modules/User.interface';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent {
  name: string = '';
  user!: UserInformation;
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UsersService
  ) {
    this.name = this.activatedRoute.snapshot.params.name;
    this.getUser();
  }
  
  getUser() {
    this.userService.getUser(this.name).then((value) => {
      this.user = value;
    });
  }
}
