import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User, UserInformation } from '../modules/User.interface';


@Injectable({
  providedIn: 'root'
})

export class UsersService {
  
  url:String = 'https://api.github.com/search/users?q=';
  userUrl:String = 'https://api.github.com/users/';
  constructor(private http: HttpClient) {}

  search(name:string): Observable<User> {
    let apiURL = `${this.url}${name}`;
    return this.http.get<User>(apiURL);
  }

  countFollowers(url:string): Observable<User[]> {
    return this.http.get<User[]>(url);
  }

  getUser(name:string){
    let promise = new Promise<UserInformation>((resolve, reject) => {
      let apiURL = `${this.userUrl}${name}`;
      this.http.get(apiURL)
        .toPromise()
        .then(
          res => { // Success
            const stringData = JSON.stringify(res, null, 2);
            const dataJson = JSON.parse(stringData);

            const user: UserInformation = {
              avatar_url : dataJson['avatar_url'],
              followers_url: dataJson['followers_url'],
              id : dataJson['id'],
              login : dataJson['login'],
              score : dataJson['score'],
              subscriptions_url : dataJson['subscriptions_url'],
              url : dataJson['url'],
              name : dataJson['name'],
              followers : dataJson['followers'],
              public_repos : dataJson['public_repos'],
              location: dataJson['location'],
              following : dataJson['following']
            }

            resolve(user);
          }
        );
    });
    return promise;
  }
}
