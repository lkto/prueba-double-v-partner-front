import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors,
  AbstractControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  ChartConfiguration,
  ChartData,
  ChartOptions,
  ChartType,
} from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { User, UserInformation } from '../modules/User.interface';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  angForm: FormGroup = this.formBuilder.group({
    name: [
      '',
      [Validators.required, Validators.minLength(4), this.ValidateExeptionWord],
    ],
  });
  submit: boolean = false;
  users: UserInformation[] = new Array<UserInformation>();
  isFind: boolean = false;
  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 0,
      },
    },
    plugins: {
      legend: {
        display: true,
      },
    },
  };

  public chartLabel: string[] = [];
  public barChartType: ChartType = 'bar';
  public barChartData: ChartData<'bar'> = {
    datasets: [{ data: [65, 59, 80, 81, 56, 55, 40], label: 'Users' }],
  };

  constructor(
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private router: Router
  ) {}

  onSubmit(): void {
    this.submit = true;

    if (this.angForm.invalid) {
      return;
    }

    this.getUsers(this.angForm.get('name')?.value);
  }

  ValidateExeptionWord(formBuilder: AbstractControl): ValidationErrors | null {
    const name = formBuilder.value;

    if (name == 'doublevpartners') return { invalidWord: true };

    return null;
  }

  getUsers(name: string) {
    this.userService.search(name).subscribe((response) => {
      this.users = response.items;
      this.dataInfo(response.items);
      this.isFind = true;
    });
  }

  openProfile(name: string, score: number) {
    localStorage.setItem('user', name);
    localStorage.setItem('score', score.toString());
    this.router.navigate(['/profile/' + name]);
  }

  dataInfo(users: UserInformation[]) {
    let label = [];
    let datai: number[] = [];

    for (let index = 0; index < 10; index++) {
      const user: UserInformation = users[index];
      label.push(user.login);
      this.userService.getUser(user.login).then((value) => {
        datai.push(parseInt(value.followers));
      });
    }

    this.barChartData.datasets[0].data = datai;
    this.barChartData.labels = label;

    this.chart?.chart?.update();

    console.log(this.chart)
  }
}
